<?php //$Id
/**
 * @file
 * Creates vocabulary sets as nodes.
 */
/**
 * Implementation of hook_help().
 *
 * Throughout Drupal, hook_help() is used to display help text at the top of
 * pages. Some other parts of Drupal pages get explanatory text from these hooks
 * as well. We use it here to provide a description of the module on the
 * module administration page.
 */
function vocab_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      // This description is shown in the listing at admin/modules.
      return t('Create a glossary of vocabulary pairs in two languages for review with other modules like the flashcard module');
    case 'node/add#vocab':
      // This description shows up when users click "create content."
      return t('A vocabulary set is a group of vocabulary pairs that are typically reviewed in some form of memory exercise. ');
  }
}

/**
 * Implementation of hook_user().
 */

function vocab_user($op, &$edit, &$user, $category = array('weight'=>-15)) {
	global $pager_page_array, $pager_total, $pager_total_items; 
	switch ($op) {
    	case 'view':
    	{//display all user vocabs
			$my  = pager_query("SELECT * FROM {node} WHERE uid='$user->uid' AND type = 'vocab'
				ORDER BY changed ASC", 10);
			while($vocab = db_fetch_object($my)){				
				$nurl = 'node/'.$vocab->nid;
				$myvocab[l('Vocabulary Sets', 'vocab/user')][] = l($vocab->title,  $nurl );
				$true = 1; 
			}			
			
			if($true){
				$myvocab[l('Vocabulary Sets', 'vocab/user')] = array(array('title'=>join($myvocab[l('Vocabulary Sets', 'vocab/user')], ', ')));
			}
			return ($myvocab);
		}  	
      	break; 
	}		
}

function _vocab_user() {
	global $user, $pager_page_array, $pager_total, $pager_total_items; 

    $js = '<script language="JavaScript">function vocab_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location=\'"+selObj.options[selObj.selectedIndex].value+"\'");
  if (restore) selObj.selectedIndex=0;}
</script>';

	drupal_set_html_head($js);
	$myvocab = array();
		//display all user vocabs
	$my  = pager_query("SELECT * FROM {node} WHERE uid='$user->uid' AND type = 'vocab'
				ORDER BY changed ASC", 10);

	
	
	while($vocab = db_fetch_object($my)){
		$nurl = url('node/'.$vocab->nid.'/');		
		$options = array();
		$options['none'] = t('Select Review Method');
		module_exist('flashcard')?( $options[$nurl.'flashcard'] = t('view as flashcards')):'';
		module_exist('typecheck')?( $options[$nurl.'typecheck'] = t('view as typechecks')):'';
		$form['myvocab'] = array(
			'#type' => 'select',
			'#title' => $vocab->title,
			'#options' => $options,
			'#default_value' => 'none',
			'#description'=>t('If there are no review methods, ask the administrator to enable a method.'),
			'#attributes' => array(
			'onChange' => "vocab_jumpMenu('self',this,0)"));	
		$myvocab['Vocabulary Sets'][]= form_render(form_builder(NULL, $form));
	}
	

	drupal_set_title(t('My Vocabulary Sets'));
	$output.= join($myvocab['Vocabulary Sets'], ''). theme_pager(array());
	return $output;
}
/**
 * Implementation of hook_node_name().
 *
 * This is a required node hook. Since our module only defines one node
 * type, we won't implement hook_node_types(), and our hook_node_name()
 * implementation simply returns the translated name of the node type.
 */
function vocab_node_info() {
	return array('vocab' => array('name' => t('Vocabulary Set'), 'base' => 'vocab'));
}

/**
 * Implementation of hook_access().
 *
 * Node modules may implement node_access() to determine the operations
 * users may perform on nodes. This vocab uses a very common access pattern.
 */
function vocab_access($op, $node) {
  global $user;

  if ($op == 'create') {
    // Only users with permission to do so may create this node type.
    return user_access('create vocab set');
  }

  // Users who create a node may edit or delete it later, assuming they have the
  // necessary permissions.
  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit own vocab sets') && ($user->uid == $node->uid)) {
      return TRUE;
    }
  }
  
  if($op == 'view')
  {
  	return user_access('view vocab set'); 
  }
}

/**
 * Implementation of hook_perm().
 *
 * Since we are limiting the ability to create new nodes to certain users,
 * we need to define what those permissions are here. We also define a permission
 * to allow users to edit the nodes they created.
 */
function vocab_perm() {
  return array('view vocab set', 'create vocab set', 'edit own vocab sets', 'delete vocab set');
}

/**
 * Implementation of hook_link().
 *
 * This is implemented so that an edit link is displayed for users who have
 * the rights to edit a node.
 */
function vocab_link($type, $node = 0) {
  $links = array();

  if ($type == 'node' && $node->type == 'vocab') {
    // Don't display a redundant edit link if they are node administrators.
    if (vocab_access('update', $node) && !user_access('administer nodes')) {
      $links[] = l(t('edit this vocab set'), "node/$node->nid/edit");
    }
  }

  return $links;
}

/**
 * Implementation of hook_menu().
 *
 * In order for users to be able to add nodes of their own, we need to
 * give them a link to the node composition form here.
 */
function vocab_menu($may_cache) {
  $items = array();

 if ($may_cache) 
 {
    $items[] = array('path' => 'node/add/vocab', 'title' => t('vocab set'),
      'access' => user_access('create vocab set'));
  }
 
  $items[] = array('path' => 'vocab', 
  'access' => user_access('create vocab set'),
  'callback' => add_vocab_pair,
  'type'=>MENU_CALLBACK);
  
  $items[] = array('path' => 'vocab/user',
  'title'=>t('My vocabulary sets'), 
  'access' => user_access('edit own vocab set'),
  'callback' => _vocab_user,
  'type'=> MENU_SUGGESTED_ITEM);


  return $items;
}


/**
 * Implementation of hook_form().
 *
 * Now it's time to describe the form for collecting the information
 * specific to this node type. This hook requires us to return some HTML
 * that will be later placed inside the form.
 */
function vocab_form(&$node) 
{  
	$form['#token'] = FALSE;
	$form['title'] = array('#type' => 'textfield',
							 '#title' => t('Subject'),
							 '#default_value' => $node->title,
							 '#size' => 60,
							 '#maxlength' => 128,
							 '#required' => TRUE,
							 '#description'=>t('The subject matter of this vocabulary set'));
  $form['body_filter'] = array('#type'=>'fieldset');
  $form['body_filter']['filter'] = filter_form($node->format);							 
  $form['body_filter']['body'] = array('#type' => 'textarea',
						  '#title' => t('Preamble'),
						  '#cols' => 60,
						  '#rows' => 5,
						  '#default_value' => $node->body,
						  '#description' => t('Preamble text.'));
$form['body_filter']['flabel'] = array('#type' => 'textfield',
						  '#title' => t('Front label'),
						  '#cols' => 60,
						  '#rows' => 5,
						  '#default_value' => $node->flabel,
						  '#description' => t('Front label.'));
$form['body_filter']['blabel'] = array('#type' => 'textfield',
						  '#title' => t('Back label'),
						  '#cols' => 60,
						  '#rows' => 5,
						  '#default_value' => $node->blabel,
						  '#description' => t('Back label.'));						  
	
	return $form;
}





/**
 * Implementation of hook_update().
 *
 * As an existing node is being updated in the database, we need to do our own
 * database updates.
 */
function vocab_update($node) {
 $args = preg_split("/\//",$_GET['q']);

  $pos = (sizeof($args)-2);
  switch( $args[$pos])
  {
  	case "edit":
	{
		if(is_numeric($args[$pos + 1]))
		{
			db_query("UPDATE {vocab_data} SET front='%s', back='%s', imageurl='%s'  WHERE id = %d ", $_POST['edit']['front'], $_POST['edit']['back'], $_POST['edit']['imageurl'], $args[$pos + 1]);
			if(db_error()) exit();
		}
		else  //add
		{
			db_query("INSERT INTO {vocab_data} (nid, front, back, imageurl) VALUES (%d, '%s', '%s', '%s')", $node->nid, $_POST['edit']['front'], $_POST['edit']['back'], $_POST['edit']['imageurl']);
		}
		 
	 }
	 break;
	 default:
	 {
	 	db_query("REPLACE INTO {vocab_labels} (nid, flabel, blabel) VALUES (%d, '%s', '%s')", $node->nid, $node->flabel, $node->blabel);
	 }
	 break;
	}
}

/**
 * Implementation of hook_delete().
 *
 * When a node is deleted, we need to clean up related tables.
 */
function vocab_delete($node) {
  db_query("DELETE FROM {vocab_data} WHERE nid = '%d'", $node->nid);
  db_query("DELETE FROM {vocab_labels} WHERE nid=%d", $node->nid);
}

/**
 * Implementation of hook_load().
 *
 * Now that we've defined how to manage the node data in the database, we
 * need to tell Drupal how to get the node back out. This hook is called
 * every time a node is loaded, and allows us to do some loading of our own.
 */
/*function vocab_load($node) {
	
	
 // $additions = db_fetch_object(db_query('SELECT * FROM {vocab_data} WHERE id = %d', $node->nid));
  return $additions; 
}*/

/**
 * Implementation of hook_view().
 *
 * This is a typical implementation that simply runs the node text through
 * the output filters.
 */
function vocab_view(&$node, $teaser = FALSE, $page = FALSE) {
  $data_info = theme('vocab_data_info', $node);
  $add_data = theme('vocab_add_data', $node);
  $node->body .= $data_info;
  $node->body .= $add_data;
  $node->teaser .= $data_info;
//  $node = node_prepare($node, $teaser); //applies selected filters to $node
}

/**
 * A custom theme function.
 *
 * By using this function to format our node-specific information, themes
 * can override this presentation if they wish. We also wrap the default
 * presentation in a CSS class that is prefixed by the module name. This
 * way, style sheets can modify the output without requiring theme code.
 */
function theme_vocab_data_info($node) {
  global $user;
  $toDisplay = variable_get('vocabset_display_number', 10);
  
  $nrows = db_num_rows(db_query('SELECT front, back FROM {vocab_data} WHERE nid = %d', $node->nid));
  $output = '<div class="vocab_data_info">';
  $output .= t('%npairs vocabulary pair(s) in this vocabulary set.', array('%npairs' => $nrows)); 

  $result = pager_query("SELECT * FROM {vocab_data} WHERE nid='$node->nid'
				ORDER BY nid ASC", $toDisplay);
  $header = array('', t('Card ID'), t($node->flabel), t($node->blabel), t('Explanation'), '');
  
if(isset($_GET['page']))
	$from = $_GET['page'];
else
	$from = 0;
	
 while($row = db_fetch_object($result)){  
  	if(user_access('edit own vocab sets') && ($user->uid == $node->uid)){
		$edit = '['.l(t("Edit"), "vocab/$node->nid/edit/$row->id").']';				
		if($_GET['op'] == "delete/".$row->id) 
			$delete = '['.l(t("Confirm"), "node/$node->nid", NULL, 'op=confirmdelete/'.$row->id.'&page='.$from).' | '.l(t("Cancel"), "node/$node->nid", NULL, 'page='.$from).']' ;
		else if($_GET['op'] == "confirmdelete/".$row->id){
				$todel = split("/", $_GET['op']);
				db_query("DELETE FROM {vocab_data} WHERE id=%d", $todel[1]); 
				$delete = '<b>Deleted</b>';
			}
		else
			$delete = '['.l(t("Delete"), "node/$node->nid", NULL, 'op=delete/'.$row->id.'&page='.$from).']';
	}
	
	//the "image" is really the explanation keeping the field names to preserve compatibility.
	//since a link to an image may be the greatest explanation of all :) 
	$image = $row->imageurl; 
	
  	$rows[] = array(
  					array('data'=>$edit),
  					array('data'=>$node->nid.'-'.$row->id),
  					array('data'=>check_plain($row->front)),
  					array('data'=>check_plain($row->back)),
  					array('data'=>check_markup($row->imageurl)),
  					array('data'=>$delete),
  				    );
  } 

	//make sure we are on the node's own page (rather than the listing of all nodes say) for the theme_pager to work
	if (arg(0) == 'node' && is_numeric(arg(1))){
		$pager .= theme_pager(array(),$toDisplay);
	}

  	if(user_access('edit own vocab sets') && ($user->uid == $node->uid)){
	  	$addvp .= l(t("Add a vocabulary pair to this set."),'vocab/'.$node->nid.'/add' ); 
	}
  $output .= '</div>';
  $output.= $addvp.theme('table', $header, $rows, array('width'=>'90%', 'align'=>'center')).$addvp.$pager;
  return $output;
}

/* Add another / Edit existing vocabulary pair to/in the vocabulary set */
function vocab_add_data_form($node) {	
	$form = array();
	$form['#token'] = FALSE;	
	switch( arg(2))
	{		
		case "add":
		{		
			$form['add'] = array(
				'#type' => 'fieldset',
				'#title' => t('Add a "Vocabulary" pair to this set.'),
				'#prefix' => '<div class="vocab_add_data"><h4 style="color:black";>',
				'#suffix' => '</h4>');		
			
				
			$form['add']['front'] = array(
				'#type' => 'textarea',
				'#title' => t($node->flabel),
				'#default_value' => $node->front,
				'#cols' => 60,
				'#rows' => 3,
				'#maxlength' => 250);			
			
			$form['add']['back'] = array(
				'#type' => 'textarea',
				'#title' => t($node->blabel),
				'#default_value' => $node->back,
				'#cols' => 60,
				'#rows' => 5,
				'#maxlength' => 250);

			$form['add']['imageurl'] = array(
				'#type' => 'textarea',
				'#title' => t('Explanation'),
				'#cols' => 60,
				'#rows' => 5,
				'#description'=>t('Feedback for this pair'),
				'#default_value' => $node->imageurl);
			
			$form['div_tag'] = array(
				'#type' => 'markup',
				'#value' => '</div> <script> </script>');
			
			$form['add']['submit'] = array(
				'#type' => 'submit',
				'#value' => t('Submit'));
		}
		break;
		case "edit":
		{ 
			if(is_numeric(arg(3)))
			{
				$form['edit'] = array(
					'#type' => 'fieldset',
					'#title' => t('Edit this "Vocabulary" pair.'),
					'#prefix' => '<div class="vocab_add_data"><h4 style="color:red";>',
					'#suffix' => '</h4>');
				$row = db_fetch_object(db_query("SELECT * FROM {vocab_data} WHERE id =%d", arg(3)));
				$node->front = $row->front;
				$node->back = $row->back;
				$node->imageurl = $row->imageurl;				
			}
			else{
				$form['edit'] = array(
					'#type' => 'fieldset',
					'#title' => t('Add a "Vocabulary" pair to this set.'),
					'#prefix' => '<div class="vocab_add_data"><h4 style="color:black";>',
					'#suffix' => '</h4>');
			}			
				
			$form['edit']['front'] = array(
				'#type' => 'textarea',
				'#title' => t($node->flabel),
				'#default_value' => $node->front,
				'#size' => 60,
				'#maxlength' => 250,
				'#required' => TRUE);
				
			$form['edit']['back'] = array(
				'#type' => 'textarea',
				'#title' => t($node->blabel),
				'#default_value' => $node->back,
				'#size' => 60,
				'#maxlength' => 250);

			$form['edit']['imageurl'] = array(
				'#type' => 'textarea',
				'#title' => t('Explanation'),
				'#cols' => 60,
				'#rows' => 5,
				'#description'=>t('Feedback for this pair.'),
				'#default_value' => $node->imageurl);

			$form['edit']['pid'] = array(
				'#type' => 'hidden',
				'#value' => arg(3));
			
			$form['div_tag'] = array(
				'#type' => 'markup',
				'#value' => '</div>');
			
			$form['edit']['submit'] = array(
				'#type' => 'submit',
				'#value' => t('Submit'));
			
		}
		break;
	}		
	return $form;	
}


function add_vocab_pair_validate($form_id, $form_values)
{
	if ('add_vocab_pair' == $form_id)
	{
		if(isset($form_value['edit']))
		{
			if($form_values['front'] == "" || $form_values['back'] == "")
			{
				form_set_error('',t('Only the Image field may be left blank'));
			}
		}	
	}
}

function add_vocab_pair_submit($form_id, $form_values)
{
	$nid = arg(1);
	$pid = arg(3);
	if(isset($nid) && $nid != 0)
	{
		if ( 'add_vocab_pair' == $form_id)
		{
			$pair = (object) array('nid'=> $nid, 'front'=>$form_values['front'], 'back'=>$form_values['back'],'imageurl'=> $form_values['imageurl']);		
			
			if(is_numeric($form_values['pid']))
				vocab_update($pair);
			else
				_vocab_insert($pair);

			drupal_set_message("[".$pair->front." | ". $pair->back."] added");
			drupal_goto('vocab/'.$nid.'/add');
		}
	}
	else
	{
		drupal_goto("node");
	}	
}

/******************** HELPER FUNCTIONS *****************************/

function add_vocab_pair()
{
	$nid = arg(1);
	$pid = arg(3);
	if(isset($nid))
	{
		$node = node_load(array('nid' => $nid)); 
		$breadcrumb[] = array('path' => 'node/'.$nid, 'title' => ($node->title));
		
		switch(arg(2)){
			case 'add':
			$breadcrumb[] = array('path' => 'vocab/'. $nid.'/add', 'title' => t("add vocab pair", array()));
			break;
			case 'edit':
			$breadcrumb[] = array('path' => 'vocab/'. $nid.'/edit/'.$pid, 'title' => t("edit vocab pair", array()));
			break;
		}
		menu_set_location($breadcrumb);
		
		//TODO: focus cursor in first box
		$add_data = vocab_add_data_form($node);
		$output = drupal_get_form('add_vocab_pair', $add_data);		
		$output .= l(t("Return to the key page"), "node/".$nid);
		
	}
	else
	{
		drupal_goto("node");
	}
	return $output;
}

function _vocab_insert($node) 
{
  db_query("INSERT INTO {vocab_data} (nid, front, back, imageurl) VALUES (%d, '%s', '%s', '%s')", $node->nid, $node->front, $node->back, $node->imageurl);
}

/**
 * hook_insert()
 */
function vocab_insert($node) {//REPLACE INTO less efficient, but solves the problem of orphaned older nodes which didn't have labels
  db_query("REPLACE INTO {vocab_labels} (nid, flabel, blabel) VALUES (%d, '%s', '%s')", $node->nid, $node->flabel, $node->blabel);
}

/**
 * hook_load
 */
 function vocab_load($node) {
 	return db_fetch_array(db_query("SELECT * FROM {vocab_labels} WHERE nid=%d", $node->nid));
 }
function vocab_settings()
{
	$form['labels']= array('#type'=>'fieldset', '#description'=>t('Vocabulary pairs display'));
	$form['labels']['vocabset_display_number'] = array (
				'#type'=>'textfield',
				'#title'=>t('Default number of pairs to display'),
				'#default_value'=>variable_get('vocabset_display_number', 10),
				);
	$form['#token'] = FALSE;		
	return $form;
}

?>